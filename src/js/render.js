import $ from 'jquery';

export function renderBasicInfo(name, age) {
  $('#name').html(name);
  $('#age').html(age);
}

export function renderAboutMe(description) {
  $('#about-me').html(description);
}

export function renderEducation(educations) {
  educations.forEach(element => {
    $('#education').append(`
        <li class="education">
           <span>${element.year}</span>
           <div>
               <h4>${element.title}</h4>
               <p>${element.description}</p>
           </div>
        </li>`);
  });
}
