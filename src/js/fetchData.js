export const URL = 'http://localhost:3000/person';

export function fetchData(url) {
  return fetch(url).then(response => response.json());
}
