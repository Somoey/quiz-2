import { fetchData, URL } from './fetchData';
import { createPerson } from './createPerson';
import { renderAboutMe, renderBasicInfo, renderEducation } from './render';

fetchData(URL).then(response => {
  const { name, age, description, educations } = createPerson(response);
  // console.info(response);
  renderBasicInfo(name, age);
  renderAboutMe(description);
  renderEducation(educations);
});
